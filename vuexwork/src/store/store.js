import Vue from 'vue';
import Vuex from 'vuex';
import { getDate } from '../request/request';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        list:[],
        cartList:[]
    },
    mutations: {
        saveDate(state, date) {
            state.list = date.data.data.categoryList;
        },
        changeCount(state, date) {
            state.list[date.ind].spuList[date.index].activityType++;
            let arr = [];
            state.list.forEach((v,i) => {
                v.spuList.filter((item,index) => {
                     item.activityType > 0 ? arr.push(item) : ''
                })
            });
            state.cartList = arr;
        },
        changeRed(state, date) {
            state.list[date.ind].spuList[date.index].activityType--;
        },
        clearCartList(state, date) {
            state.cartList = date;
            for(var i = 0; i < state.list.length; i++) {
                for(var k = 0; k < state.list[i].spuList.length; k++) {
                    state.list[i].spuList[k].activityType = 0;
                }
            }
        }
    },
    actions: {
        async getDate(context) {
            context.commit('saveDate', await getDate())
        }
    }
})